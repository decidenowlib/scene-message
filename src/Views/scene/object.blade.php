<script>
SceneClass = (function (original) {
	SceneClass = function SceneClass() {
		original.apply(this, arguments);
		
		this.alertScene = function() {
			var alert_root = this.getElement('[data-scene-id$="message-alert"]');
			if (alert_root.length) {
				return DecideNowObjects.stage.getScene(alert_root.data('scene-id'))
			}
			return null;
		}
		
	}
	SceneClass.prototype = original.prototype;
	SceneClass.prototype.constructor = SceneClass;
    return SceneClass;
})(SceneClass);

SceneClass = (function (original) {
	SceneClass = function Scene() {
		original.apply(this, arguments);
		var _parent = new original();
		
		this.showMessages = function (messages, message_type, alert_errors, has_content) {
			var alert_scene = this.alertScene();
			if (!alert_scene) {
				_parent.showMessages(messages, message_type, alert_errors, has_content);
				return;
			}
			if (has_content) {
				return;
			}
			if ( messages.success && (typeof message_type === 'undefined' || message_type === 'success') ) {
				for (msg_key in messages.success) {
					var real_msg = messages.success[msg_key];
					alert_scene.showSuccess(real_msg);
				}
			}
			if ( messages.warning && (typeof message_type === 'undefined' || message_type === 'warning') ) {
				for (msg_key in messages.warning) {
					var real_msg = messages.warning[msg_key];
					alert_scene.showWarning(real_msg);
				}
			}
			if ( messages.error && (typeof message_type === 'undefined' || message_type === 'error') ) {
				for (msg_key in messages.error) {
					var real_msg = messages.error[msg_key];
					alert_scene.showError(real_msg);
				}
			}
		}
		
	}
	SceneClass.prototype = original.prototype;
	SceneClass.prototype.constructor = SceneClass;
    return SceneClass;
})(SceneClass);
</script>