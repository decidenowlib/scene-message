<div class="row"><div class="col{{ config('scene.use_bootstrap_3', 0) ? '-xs-12' : '' }}">

	@php
		$messages_all = ($scene_parent) ? $scene_parent->messages->toArray() : $messages->toArray();
		$messages_success = array_key_exists('success', $messages_all) ? $messages_all['success'] : [];
		$messages_warning = array_key_exists('warning', $messages_all) ? $messages_all['warning'] : [];
		$messages_error = array_key_exists('error', $messages_all) ? $messages_all['error'] : [];
	@endphp

	<div id="alerts-success">	
		@foreach ($messages_success as $message)
			<div class="alert alert-success alert-dismissible{{ !config('scene.use_bootstrap_3', 0) ? ' fade ' : ' ' }}show">
				{!! $message !!}{!! !config('scene.use_bootstrap_3', 0) ? '<br>' : '' !!}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endforeach
	</div>
	
	<div id="alerts-warning">
		@foreach ($messages_warning as $message)
			<div class="alert alert-warning alert-dismissible{{ !config('scene.use_bootstrap_3', 0) ? ' fade ' : ' ' }}show">
				{!! $message !!}{!! !config('scene.use_bootstrap_3', 0) ? '<br>' : '' !!}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endforeach
	</div>
	
	<div id="alerts-error">
		@foreach ($messages_error as $message)
			<div class="alert alert-danger alert-dismissible{{ !config('scene.use_bootstrap_3', 0) ? ' fade ' : ' ' }}show">
				{!! $message !!}{!! !config('scene.use_bootstrap_3', 0) ? '<br>' : '' !!}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endforeach
	</div>
	
</div></div>