<script>
new DecideNowObjects.SceneClass('{{ $scene->sceneId() }}', {

	clearSuccess : function() {
		var thisScene = this;
		thisScene.getElement('[id="alerts-success"] .alert').each(function() {
			$(this).alert('close');
		});
	},

	clearWarning : function() {
		var thisScene = this;
		thisScene.getElement('[id="alerts-warning"] .alert').each(function() {
			$(this).alert('close');
		});
	},
	
	clearError : function() {
		var thisScene = this;
		thisScene.getElement('[id="alerts-error"] .alert').each(function() {
			$(this).alert('close');
		});
	},

	clearAlerts : function() {
		var thisScene = this;
		thisScene.clearSuccess();
		thisScene.clearWarning();
		thisScene.clearError();
	},
	
	showAlert : function (msg, disable_close, type) {
		var thisScene = this;
		var div = $('<div>', { 
			'class': 'alert alert-' + ((type === 'error') ? 'danger' : type)  + ' alert-dismissible{{ !config('scene.use_bootstrap_3', 0) ? ' fade ' : ' ' }}show',
			'html': msg 
		});
		if (!disable_close) {
			div.append(
				$('<button>', { 
					'type': 'button', 
					'class': 'close', 
					'data-dismiss': 'alert', 
					'aria-label': 'close' 
				}).append(
					$('<span>', { 
						'aria-hidden': 'true', 
						'html': '&times;' 
					})
				)
			);
		}
		thisScene.getElement('[id="alerts-' + type + '"]').append(div);
	},
	
	showSuccess : function(msg, disable_close) {
		var thisScene = this;
		thisScene.showAlert(msg, disable_close, 'success');
	},
	
	showWarning : function(msg, disable_close) {
		var thisScene = this;
		thisScene.showAlert(msg, disable_close, 'warning');
	},

	showError : function(msg, disable_close) {
		var thisScene = this;
		thisScene.showAlert(msg, disable_close, 'error');
	},
	
}).init();
</script>