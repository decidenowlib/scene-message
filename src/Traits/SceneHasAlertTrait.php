<?php

namespace DecideNow\SceneMessage\Traits;

use DecideNow\SceneMessage\Controllers\SceneMessageAlertController;

trait SceneHasAlertTrait
{	
	public function childAddAlert()
	{
		$this->childAdd(new SceneMessageAlertController($this), 'message-alert');
	}
}