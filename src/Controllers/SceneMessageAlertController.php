<?php

namespace DecideNow\SceneMessage\Controllers;

use DecideNow\Scene\Controllers\SceneBaseController;

class SceneMessageAlertController extends SceneBaseController
{	
	public static $scene_key = 'scene-message-alert';
	
	public function __construct($scene_parent = null, $scene_id = '', $scene_id_suffix = '')
	{
		parent::__construct($scene_parent, $scene_id, $scene_id_suffix);
		$this->has_content = true;
		$this->template_content = 'scene-message::alert.message-alert_content';
		$this->template_code = 'scene-message::alert.message-alert_code';
	}
}