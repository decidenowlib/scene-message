<?php

namespace DecideNow\SceneMessage;

use Illuminate\Support\ServiceProvider;

class SceneMessageServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
		//
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
		$this->loadViewsFrom(__DIR__.'/Views/scene', 'scene.message');
		$this->loadViewsFrom(__DIR__.'/Views/message', 'scene-message');
		$this->mergeConfigFrom(__DIR__.'/Config/scene.php', 'scene.extensions');
    }
}
