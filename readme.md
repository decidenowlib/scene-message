# Scene Message #

## Описание ##
Расширение для библиотеки Scene для Laravel, позволяющее отображать сообщения (alert) при загрузке и при помощи кода.

## Установка ##
Исходный код проекта "живёт" на Bitbucket, поэтому для начала необходимо подключить репозиторий, а затем добавить пакет:

```
composer config repositories.decidenow.scene.message vcs https://bitbucket.org/decidenowlib/scene-message
composer require decidenow/scene-message
```

## Начало работы ##
В контроллере сцены, для которой будут отображаться сообщения, необходимо подключить Trait с дополнительным функционалом и добавить определение дочерней сцены

```
...
use DecideNow\SceneMessage\Traits\SceneHasAlertTrait;
...
class ...
{
	...
use SceneHasAlertTrait;
...
public function childrenDefine()
{
	$this->childAddAlert();
	...
}
	...
}
```

В представлении сцены в нужном месте разместить код вставки блока с соощениями

```
...
@include('scene-message::alert')
...
```

При необходимости в коде сцены можно использовать функции:
```
thisScene.alertScene().clearSuccess();
thisScene.alertScene().clearWarning();
thisScene.alertScene().clearError();
thisScene.alertScene().clearAlerts();

thisScene.alertScene().showSuccess('Успех!');
thisScene.alertScene().showWarning('Внимание!');
thisScene.alertScene().showError('Ошибка!');
```


## Автор ##
Соколов Александр

E-mail: sanya_sokolov@inbox.ru
